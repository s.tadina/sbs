package com.example.Abschluss.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter

@Entity (name = "Favorite")
public class FavoriteModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int FavoriteId;

    @ManyToMany (mappedBy = "favoriteModel")
    private Set<RecipeModel> recipeModelSet = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "user")
    private UserModel userModel;

    public FavoriteModel() {}

    public FavoriteModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public void addRecipeSet(RecipeModel recipeModel) {recipeModelSet.add(recipeModel);}




}
