package com.example.Abschluss.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnJava;

import java.sql.Time;
import java.util.Date;

@Getter
@Setter
@Entity

public class CommentModel {

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private int commentId;

    @Column
    private String firstName;

    @Column
    private Date date;

    @Column
    private Time time;

    @Column
    private String comment;

    @ManyToOne
    @JoinColumn(name = "recipeId")
    RecipeModel recipeModel;

    @ManyToOne
    @JoinColumn(name = "userId")
    UserModel userModel;

    public CommentModel() {}

    public CommentModel(String firstName, Date date, Time time, String comment, RecipeModel recipeModel, UserModel userModel) {
        this.firstName = firstName;
        this.date = date;
        this.time = time;
        this.recipeModel = recipeModel;
        this.userModel = userModel;

    }


}
