package com.example.Abschluss.model;

import com.example.Abschluss.enums.Category;
import com.example.Abschluss.enums.Unit;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity(name = "recipe_ingredient")
public class RecipeIngredientModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int recipeIngredientId;

    @Column
    private int quantity;

    @ManyToOne
    @JoinColumn(name = "ingredientId")
    private IngredientModel ingredientModel;

    @ManyToOne
    @JoinColumn(name = "recipeId")
    private RecipeModel recipeModel;

    @Column
    @ElementCollection
    @Enumerated(EnumType.STRING)
    @CollectionTable(name = "RecipeIngredientUnit")
    Set<Unit> unitSet = new HashSet<>();

    public RecipeIngredientModel() {
    }

    public RecipeIngredientModel(int quantity, IngredientModel ingredientModel, RecipeModel recipeModel) {
        this.quantity = quantity;
        this.ingredientModel = ingredientModel;
        this.recipeModel = recipeModel;
    }

    public void addUnitSet(Unit unit) {this.unitSet.add(unit);}
}


