package com.example.Abschluss.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "rating")
public class RatingModel {

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private int RatingId;

    @Column
    private String name;

    @ManyToOne
    @JoinColumn(name = "RecipeId")
    private RecipeModel recipeModel;

    @ManyToOne
    @JoinColumn(name = "userId")
    private UserModel userModel;

    public RatingModel() {}

    public RatingModel(String name, RecipeModel recipeModel, UserModel userModel) {
        this.name = name;
        this.recipeModel = recipeModel;
        this.userModel = userModel;
    }






}
