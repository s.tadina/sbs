package com.example.Abschluss.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import javax.lang.model.util.SimpleTypeVisitor7;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter

@Entity
public class UserModel {

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private int userId;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String email;

    @Column
    private String password;

    @OneToOne (mappedBy = "userModel")
    private GroceryListModel groceryListModel;

    @OneToMany(mappedBy = "userModel")
    private Set<FavoriteModel> favoriteModelSet = new HashSet<>();

    @OneToMany(mappedBy = "userModel")
    private Set<RatingModel> ratingModelSet = new HashSet<>();

    @OneToMany(mappedBy = "userModel")
    private Set<CommentModel> commentModelSet = new HashSet<>();

    public UserModel() {
    }

    public UserModel(String firstName, String lastName, String email, String password, GroceryListModel groceryListModel) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.groceryListModel = groceryListModel;
    }

    public void addFavoriteSet(FavoriteModel favoriteModel) {favoriteModelSet.add(favoriteModel);}
    public void addRatingSet(RatingModel ratingModel) {ratingModelSet.add(ratingModel);}
    public void addCommentSet(CommentModel commentModel) {commentModelSet.add(commentModel);}



}
