package com.example.Abschluss.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity(name = "ingredients")
public class IngredientModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int ingredientId;

    @Column
    private String name;

    @OneToMany(mappedBy = "ingredientModel")
    private Set<RecipeIngredientModel> recipe_ingredientModelSet = new HashSet<>();

    @ManyToMany(mappedBy = "ingredientModel")
    private Set<GroceryListModel> groceryListModelSet = new HashSet<>();


    public IngredientModel(String name) {
        this.name = name;
    }

    public IngredientModel() {}

    public void addRecipeIngredientSet(RecipeIngredientModel recipe_ingredientModel)
    {recipe_ingredientModelSet.add(recipe_ingredientModel);
    }
    public void addGroceryListSet(GroceryListModel groceryListModel) {groceryListModelSet.add(groceryListModel);
    }



}
