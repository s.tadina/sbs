package com.example.Abschluss.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity(name = "groceryList")
public class GroceryListModel {

    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    private int groceryListId;

    @Column
    private int quantity;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "userId")
    private UserModel userModel;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name="grocery_ingredients",
            joinColumns = {@JoinColumn(name = "groceryListId")},
            inverseJoinColumns = {@JoinColumn(name = "ingredientsId")}
    )
    private Set<IngredientModel> ingredientsModelSet= new HashSet<>();

    public GroceryListModel(int quantity) {
        this.quantity = quantity;
    }

    public GroceryListModel(){}

    public void addIngredientsSet(IngredientModel ingredientsModel) {ingredientsModelSet.add(ingredientsModel);}

}


