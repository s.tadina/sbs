package com.example.Abschluss.model;

import com.example.Abschluss.enums.Category;
import com.example.Abschluss.enums.PrepTime;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity(name = "recipes")
public class RecipeModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int recipeId;
    @Column
    private String name;
    @Column
    private String instructions;
    @Enumerated(EnumType.STRING)
    PrepTime prepTime;

    @Enumerated(EnumType.STRING)
    Category category;

    @OneToMany (mappedBy = "recipesModel")
    private Set<RecipeIngredientModel> recipe_ingredientModelSet = new HashSet<>();

    @OneToMany (mappedBy = "recipeModel")
    private Set<RatingModel> ratingModelSet = new HashSet<>();

    @OneToMany (mappedBy = "recipeModel")
    private Set<CommentModel> commentModelSet = new HashSet<>();

    @ManyToMany (cascade = CascadeType.ALL)
    @JoinTable(
            name = "RecipeFavorite",
            joinColumns = {@JoinColumn(name = "recipeId")},
            inverseJoinColumns = {@JoinColumn(name = "favoriteId")}
    )
    private Set<FavoriteModel> favoriteModelSet = new HashSet<>();

    public RecipeModel() {}

    public RecipeModel(String name, String instructions, PrepTime prepTime, Category category) {
        this.name = name;
        this.instructions = instructions;
        this.prepTime = prepTime;
        this.category = category;
    }

    public void addRecipeIngredientsSet(RecipeIngredientModel recipe_ingredientsModel)
    {recipe_ingredientModelSet.add(recipe_ingredientsModel);
    }

    public void addCommentSet(CommentModel commentModel) {commentModelSet.add(commentModel);}

    public void addRatingSet(RatingModel ratingModel) {ratingModelSet.add(ratingModel);}

    public void addFavoriteSet(FavoriteModel favoriteModel) {favoriteModelSet.add(favoriteModel);}







}
