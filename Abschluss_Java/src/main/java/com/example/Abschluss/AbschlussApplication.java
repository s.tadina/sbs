package com.example.Abschluss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AbschlussApplication {

	public static void main(String[] args) {
		SpringApplication.run(AbschlussApplication.class, args);
	}

}
